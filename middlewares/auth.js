const jwt = require('jsonwebtoken');
const User = require('../models/userModel');
const catchAsyncError = require('../middlewares/catchAsyncErrors');
const ErrorHandler = require('../utils/errorHandler');

// check if the user is authenticated or not

const isUserAuthenticated = catchAsyncError(async (req,res,next)=>{
    let token;

    if(req.headers.authorization  && req.headers.authorization.startsWith('Bearer')){
        token = req.headers.authorization.split(' ')[1];
    }

    if(!token){
        return next(new ErrorHandler(401,'login first to access this resource.'));
    }

    const decoded = jwt.verify(token,process.env.JWT_SECRET);
    req.user = await User.findById(decoded.id);

    next();
})

// Handling user's roles.

const authorizeRoles = (...roles) => {
    return (req,res,next)=>{
        if(!roles.includes(req.user.role)){
            return next(new ErrorHandler(403,`Role(${req.user.role}) is not allowed to access this resource.`))
        }
        next();
    }
}



module.exports = {
    isUserAuthenticated,
    authorizeRoles};