const ErrorHandler = require('../utils/errorHandler');


module.exports = (err, req, res, next) => {
    err.statusCode = err.statusCode || 500;

    if (process.env.NODE_ENV === 'development') {

        res.status(err.statusCode).json({
            success: false,
            error: err,
            errMessage: err.message,
            stack: err.stack
        });

    } else if (process.env.NODE_ENV === 'production') {

        let error = { ...err };
        error.message = err.message;

        // Wrong mongoose object ID error.

        if(err.name === 'CasteError'){
            const message = `Resource not found, Invalid : ${err.path} `;
            error = new ErrorHandler(404, message);
        }

        // Handling Mongoose Validation Error.

        if(err.name === 'ValidationError'){
            const message = Object.values(err.errors).map(value => value.message);
            error = new ErrorHandler(400, message);
        }

        // Handling mongoose Duplicate Key Error.
        if(err.code === 11000){
            const message = `Duplicate ${Object.keys(err.keyValue)} entered.`;
            error = new ErrorHandler(400,message);
        }


        // Handling wrong JWT token error.
        if(err.name === 'JsonWebTokenError'){
            const message = 'JSON web token is invalid. Try Again.';
            error = new ErrorHandler(500,message);
        }

        // Handling expired JWT token error
        if(err.name === 'TokenExpiredError'){
            const message = 'JSON web token has expired. Login Again.';
            error = new ErrorHandler(500,message);
        }

        res.status(error.statusCode).json({
            success: false,
            message: error.message || 'Internal Server Error.'
        });
    }
};
