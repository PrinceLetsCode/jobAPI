// Create and send token and save in cookie

const sendToken = (user,statusCode,res)=>{
    // crate JWT token.

    const token = user.getJwtToken();

    const expiresIn = process.env.COOKIE_EXPIRES_TIME || 24 * 60 * 60 * 1000;

    const expirationDate = new Date(Date.now() + expiresIn);

    //options for cookie.
    const options ={
        expire : expirationDate,
        httpOnly : true
    };


    // if(process.env.NODE_ENV === 'production'){
    //     options.secure = true;
    // }    

    res
        .status(statusCode)
        .cookie('token',token,options)
        .json({
        success : true,
        token : token
    });
}

module.exports = sendToken;