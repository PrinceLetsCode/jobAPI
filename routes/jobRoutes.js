const express = require('express');
const router = express.Router();
const {
    isUserAuthenticated,
    authorizeRoles
    } = require('../middlewares/auth');

const { 
    getJobs,
    newJob,
    getJobsInRadius,
    updateJob,
    deleteJob,
    getJob,
    jobStats,
    applyJob
} = require('../controllers/jobController');

router.route('/jobs').get(getJobs);
router.route('/job/:id/:slug').get(getJob);
router.route('/jobs/new').post(isUserAuthenticated,authorizeRoles('employer','admin'),newJob);
router.route('/jobs/:zipcode/:distance').get(getJobsInRadius);
router.route('/job/:id')
                .put(isUserAuthenticated,authorizeRoles('employer','admin'),updateJob)
                .delete(isUserAuthenticated,authorizeRoles('employer','admin'),deleteJob);
router.route('/stats/:topic').get(jobStats);
router.route('/job/:id/apply').put(isUserAuthenticated,authorizeRoles('user'),applyJob);


// added a comment

module.exports = router;