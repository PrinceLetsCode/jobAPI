const express = require('express');

const { 
    getUserProfile, 
    updatePassword, 
    updateData,
    deleteCurrentUser,
    getAppliedJobs,
    getPublishedJobs,
    getUsers} = require('../controllers/userController');
    
const { isUserAuthenticated,authorizeRoles } = require('../middlewares/auth');

const router = express.Router();



router.route('/profile').get(isUserAuthenticated,getUserProfile);
router.route('/password/update').put(isUserAuthenticated,updatePassword);
router.route('/updateData').put(isUserAuthenticated,updateData);
router.route('/deleteUser').delete(isUserAuthenticated,deleteCurrentUser);
router.route('/jobs/applied').get(isUserAuthenticated,authorizeRoles('user'),getAppliedJobs);
router.route('/jobs/published').get(isUserAuthenticated,authorizeRoles('employer','admin'),getPublishedJobs);


// admin only routes

router.route('/users').get(isUserAuthenticated,authorizeRoles('admin'),getUsers);

module.exports = router;