const express = require('express');
const router = express.Router();

const {
    registerUser,
    loginUser,
    forgotPassword,
    resetPassword,
    logoutUser
} = require('../controllers/authController');

const {
    isUserAuthenticated,
    } = require('../middlewares/auth');


router.route('/register').post(registerUser);
router.route('/login').post(loginUser);
router.route('/password/reset').post(forgotPassword);
router.route('/password/reset/:token').put(resetPassword);
router.route('/logout').get(isUserAuthenticated,logoutUser);

module.exports = router;

