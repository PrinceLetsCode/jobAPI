const User = require('../models/userModel');
const catchAsyncErrors = require('../middlewares/catchAsyncErrors');
const ErrorHandler = require('../utils/errorHandler');
const sendToken = require('../utils/jwtToken');
const Job = require('../models/jobModel');
const fs = require('fs');
const APIFilters = require('../utils/apiFilters');


// Get current user profile => /api/v1/profile.

const getUserProfile = catchAsyncErrors( async(req,res,next)=>{

    const user = await User.findById(req.user.id)
                    .populate({
                        path : 'jobPublished',
                        select : 'title postingDate'
                    });
    
    res.status(200).json({
        success : true,
        data : user
    });
});


// update Current user password => /api/v1/password/update

const updatePassword = catchAsyncErrors(async (req,res,next)=>{
    const user = await User.findById(req.user.id).select('+password');

    // check Previous User Password.
    
    const isMatched = await user.comparePassword(req.body.currentPassword);
    if(!isMatched){
        return next(new ErrorHandler(401,'Password is incorrect.'));
    }


    user.password = req.body.newPassword;
    await user.save();

    sendToken(user,200,res);
});


// Update current user's data/details => /api/v1/updateData.

const updateData = catchAsyncErrors(async (req,res,next)=>{
    const newUserData = {
        name : req.body.name,
        email : req.body.email,
    };

    const user = await User.findByIdAndUpdate(req.user.id,newUserData,{
        new : true,
        runValidators : true,
        useFindAndModify : false
    });


    res.status(200).json({
        success : true,
        data : user
    });

});


// show all applied jobs => /api/v1/jobs/applied

const getAppliedJobs = catchAsyncErrors( async (req,res,next) => {
    const jobs = await Job.find({'applicant.id': req.user.id}).select('+applicantApplied');

    res.status(200).json({
        success : true,
        results : jobs.length,
        data : jobs
    });
    
})


// Show all jobs published by an employer

const getPublishedJobs = catchAsyncErrors (async (req,res,next)=>{
    const jobs = await Job.find({user : req.user.id});

    res.status(200).json({
        success : true,
        results : jobs.length,
        data : jobs
    });
});



// Delete current user => /api/v1/deleteUser
const deleteCurrentUser = catchAsyncErrors (async (req,res,next)=>{

    deleteUserData(req.user.id,req.user.role);

    const user = await User.findByIdAndDelete(req.user.id);

    res.cookie('token','none',{
        expire : new Date(Date.now()),
        httpOnly : true
    });


    res.status(200).json({
        success : true,
        message : 'Your account has been deleted.'
    })
})


// deleting user data.

async function deleteUserData(user,role){
    if(role === 'employer'){
        await Job.deleteMany({user : user});
    }


    if(role === 'user'){
        const appliedJobs = await Job.find({'applicant.id': user}).select('+applicantApplied');
         
        for(let i = 0; i< appliedJobs.length; i++){
            let obj = appliedJobs[i].applicantApplied.find(o => o.id === user);

            console.log(__dirname);
            let filePath = `${__dirname}/public/uploads/${obj.resume}`.replace('\\controllers','');
            
            fs.unlink(filePath,err=>{
                if(err) return console.log(err);
            });

            appliedJobs[i].applicantApplied
                .splice(appliedJobs[i].applicantApplied.indexOf(obj.id));

            appliedJobs[i].save();
            
        }
    }
}


// adding controller methods that are only accessible by admins.

// show all user => /api/v1/users

const getUsers = catchAsyncErrors( async (req,res,next)=>{
    const apiFilters = new APIFilters(User.find(),req.query)
                       .filter()
                       .sort()
                       .limitFields()
                       .pagination();


    const users = await apiFilters.query;

    res.status(200).json({
        success : true,
        results : users.length,
        data : users
    })
})



module.exports = {
    getUserProfile,
    updatePassword,
    updateData,
    deleteCurrentUser,
    getAppliedJobs,
    getPublishedJobs,
    getUsers 
}