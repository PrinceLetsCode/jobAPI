const Job = require("../models/jobModel");
const geoCoder = require('../utils/geocoder');
const ErrorHandler = require("..//utils/errorHandler");
const catchAsyncErrors = require('../middlewares/catchAsyncErrors');
const APIFilters = require('../utils/apiFilters');
const path = require('path');





const getJobs = catchAsyncErrors (async (req, res, next) => {

    const apiFilters = new APIFilters(Job.find(),req.query)
                        .filter()
                        .sort()
                        .limitFields()
                        .searchByQuery()
                        .pagination();
    
    
    const jobs = await apiFilters.query;

    if(!jobs){
        return next(new ErrorHandler(200,'No jobs for now.'));
    }

    res.status(200).json({
        success: true,
        results: jobs.length,
        data: jobs
    });
});

const newJob = catchAsyncErrors( async (req, res, next) => {


    // Adding user to body
    req.body.user = req.user.id;
    const job = await Job.create(req.body);

    if(!job) {
        return next(new ErrorHandler(404,'Job not created.'));
    }

    res.status(200).json({
        success: true,
        message: "Job created",
        data: job
    });

});


// Search jobs within radius => /api/v1/jobs/:zipcode/:distance

const getJobsInRadius = catchAsyncErrors (async (req, res, next) => {
    const { zipcode, distance } = req.params;

    // Getting latitude and longitude from geocoder with zipcode.
    const loc = await geoCoder.geocode(zipcode);

    const latitude = loc[0].latitude; // Fix the typo here (from latidute to latitude)
    const longitude = loc[0].longitude;

    const radius = parseFloat(distance) / 3963; // Parse distance as a number

    const jobs = await Job.find({
        location: {
            $geoWithin: {
                $centerSphere: [[longitude, latitude], radius],
            },
        },
    });

    res.status(200).json({
        success: true,
        results: jobs.length,
        data: jobs,
    });
});


// Update a job => /api/v1/job/:id

const updateJob = catchAsyncErrors (async (req, res, next) => {


    let job = await Job.findById(req.params.id);

    if (!job) {
        return next(new ErrorHandler(404,'Job not found.'));
    }

    job = await Job.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
        runValidators: true,
        useFindAndModify: false,
    });

    res.status(200).json({
        success: true,
        message: 'Job is updated',
        data: job
    })

});


// delete a job

const deleteJob = catchAsyncErrors ( async (req, res, next) => {
    let job = await Job.findById(req.params.id);

    if (!job) {
        return next(new ErrorHandler(404,'Job not found.'));
    }

    job = await Job.findByIdAndDelete(req.params.id);
    res.status(200).json({
        success: true,
        message: 'Job is deleted.'
    })
});


// get a single job with id and slug. => /api/v1/job/:id/:slug


const getJob = catchAsyncErrors( async (req, res, next) => {
    let job = await Job.find(
        {
            $and:
                [
                    { _id: req.params.id },
                    { slug: req.params.slug }
                ]
        }
    );

    if (!job || job.length === 0) {
        return next(new ErrorHandler(404,'Job not found.'));
    }

    job = await res.status(200).json({
        success: true,
        data: job

    })
});


// Get stats about a topic ( job ) => /api/v1/stats/:topic


const jobStats = catchAsyncErrors( async (req, res, next) => {
    const stats = await Job.aggregate([
        {
            $match: { $text: { $search: "\"" + req.params.topic + "\"" } }
        },
        {
            $group: {
                _id: { $toUpper: '$experience' },
                totalJobs: { $sum: 1 },
                avgPositions: { $avg: '$positions' },
                avgSalary: { $avg: '$salary' },
                minSalary: { $min: '$salary' },
                maxSalary: { $max: '$salary' }
            }
        }
    ]);

    if (stats.length === 0) {

        return next(new ErrorHandler(200,`No stats found for - ${req.params.topic}`));
    }


    res.status(200).json({
        success: true,
        data: stats
    })
});


// Apply job using resume. => /api/v1/job/:id/apply

const applyJob = catchAsyncErrors( async(req,res,next)=>{
    let job = await Job.findById(req.params.id).select('+applicantApplied');

    if(!job){
        return next(new ErrorHandler(404,'Job not found.'));
    }

    // check that if job last date has been passed or not.
    if(job.lastDate < new Date(Date.now())){
        return next(new ErrorHandler(400,'You cannot apply to this job, Job has expire'));
    }

    // check if user has applied before.

    for(let i = 0; i<job.applicantApplied.length; i++){
        if(job.applicantApplied[i].id === req.user.id){
            return next(new ErrorHandler(400,'You have already applied to this job'));
        }
    }

    //check the files.
    if(!req.files){
        return next(new ErrorHandler(400,'Please upload file.'));
    }

    const file = req.files.file;

    // check file type

    const supportedFiles = /.docs|.pdf/;

    if(!supportedFiles.test(path.extname(file.name))){
         return next(new ErrorHandler(400,'Please upload document file.'))
    }

    // check document size
    if(file.size > process.env.MAX_FILE_SIZE){
        return next(new ErrorHandler(400,'Please upload file less than 2MB'));
    }

    // Renaming document/Resume

    file.name = `${req.user.name.replace(' ','_')}_${job._id}${path.parse(file.name).ext}`;

    file.mv(`${process.env.UPLOAD_PATH}/${file.name}`,async (err)=>{
        if(err){
            console.log(err);
            return next(new ErrorHandler(500,'Resume upload failed.'));
        }

        await Job.findByIdAndUpdate(req.params.id, {$push :{
            applicantApplied : {
                id : req.user.id,
                resume : file.name
            }
        }},
        {
            new : true,
            runValidators : true,
            useFindAndModify : false
        });
        


        res.status(200).json({
            success : true,
            message : 'Applied to job successfully.',
            data : file.name
        })
    })



    
    
});



module.exports = {
    getJobs,
    newJob,
    getJobsInRadius,
    updateJob,
    deleteJob,
    getJob,
    jobStats,
    applyJob
};
