const User = require('../models/userModel');
const catchAsyncError = require('../middlewares/catchAsyncErrors');
const ErrorHandler = require('../utils/errorHandler');
const sendToken = require('../utils/jwtToken');
const sendEmail = require('../utils/sendEmail');
const crypto = require('crypto');


// Register a new user => /api/v1/register

const registerUser = catchAsyncError (async (req,res,next)=>{
    const {name,email,role,password} = req.body;


    const user = await User.create({
        name,
        email,
        password,
        role
    });


    sendToken(user,200,res);

})


// Login user => api/v1/login

const loginUser = catchAsyncError( async (req,res,next)=>{
    const {email, password} = req.body;

    // checks if email or password is entered or not.
    if(!email || !password){ 
        return next(new ErrorHandler(400,'Please Enter Email and Password.')); 
    }

    // finding user in database.
    const user = await User.findOne({email}).select('+password');

    if(!user){
        return next(new ErrorHandler(404,'Invalid Email or Password.',401));
    }

    //check if password is correct.
    const isPasswordMatched = await user.comparePassword(password);

    if(!isPasswordMatched){
    return next(new ErrorHandler(401,'Invalid Email or Password.'));
    }

    // create JSON web token.
    // const token = user.getJwtToken();
    // res.status(200).json({
    //     success:true,
    //     token: token
    // })

    sendToken(user,200,res);
});



// forgot password => /api/v1/password/forgot


const forgotPassword = catchAsyncError( async (req,res,next)=>{
    const user = await User.findOne({email : req.body.email});

    // check user email in database.
    if(!user){
        return next(new ErrorHandler(404,'No user found with this email.'));
    }

    // get Reset token.

    const resetToken = user.getResetPasswordToken();


    await user.save({validateBeforeSave : false});

    // Create reset password url
    const resetURL = `${req.protocol}://${req.get('host')}/api/v1/password/reset/${resetToken}`

    const message = `Your password reset link is as follow : \n\n${resetURL}\n\n if you have not requested this, then please ignore this.`;


    try {
        await sendEmail({
            email : user.email,
            subject : 'Jobbee-API Password Recovery.',
            text : message
    
        });
    } catch (error) {
        user.resetPasswordToken = undefined;
        user.resetPasswordExpire = undefined;

        console.log(error);

        await user.save({
            validateBeforeSave : false,    
        });

        return next(new ErrorHandler(500,'Email is not sent.'));
    }
 

    res.status(200).json({
        success : true,
        message : `Email sent successfully to : ${user.email}`
    })
});


// Reset Password => /api/v1/password/reset/:token

const resetPassword = catchAsyncError(async(req,res,next)=>{
    // Hash url token
    const resetPasswordToken = crypto
            .createHash('sha256')
            .update(req.params.token)
            .digest('hex');

    const user = await User.findOne({
                    resetPasswordToken,
                    resetPasswordExpire : {$gt : Date.now() }
                })


    if(!user){
        return next(new ErrorHandler(400,'Password reset token is invalid.'));
    }

    // Setup new password
    user.password = req.body.password;
    user.resetPasswordToken = undefined;
    user.resetPasswordExpire = undefined;
    await user.save();

    sendToken(user,200,res);
});


// Logout => /api/v1/logout

const logoutUser = catchAsyncError(async(req,res,next)=>{
    res.cookie('token','none',{
        expire : new Date(Date.now()),
        httpOnly : true
    });

    res.status(200).json({
        success : true,
        message : 'Logged out successfully.'
    })
})

module.exports = {
    registerUser,
    loginUser,
    forgotPassword,
    resetPassword,
    logoutUser
}



