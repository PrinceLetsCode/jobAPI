const express = require('express');
const app = express();

const dotenv = require('dotenv');
const cookieParser = require('cookie-parser');
const fileUpload = require('express-fileupload');


dotenv.config({ path: './config/config.env' });

const connectDB = require('./config/database');
const errorMiddleware = require('./middlewares/errors');

// handling uncaught Exception.
process.on('uncaughtException', err => {

    console.log(`Error : ${err.message}`);
    console.log('Shutting down due to uncaught Exception.');
    process.exit(1);
})

app.use(express.json());


// set cookie parser
app.use(cookieParser());

// Handle file uploads
app.use(fileUpload());


// importing all routes.
const jobRoutes = require('./routes/jobRoutes');
const authRoutes = require('./routes/authRoutes');
const userRoutes = require('./routes/userRoutes');
const ErrorHandler = require('./utils/errorHandler');


app.use('/api/v1', jobRoutes);
app.use('/api/v1', authRoutes);
app.use('/api/v1', userRoutes);


app.use('*', (req, res, next) => {
    next(new ErrorHandler(404, `${req.originalUrl} route not found`));
})

// Middleware to handle errros.
app.use(errorMiddleware);



const PORT = process.env.PORT || 8000;

const start = async () => {
    try {
        await connectDB(process.env.DB_LOCAL_URI);
        app.listen(PORT, () => {
            console.log(`server running on port ${PORT} in ${process.env.NODE_ENV}`);
        })
    } catch (error) {
        console.log(error);
    }
}

start();

